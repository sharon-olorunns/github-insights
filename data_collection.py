import json
import requests
import numpy as np
import pandas as p

import requests
from requests.auth import HTTPBasicAuth

# read from the credentials.json file and use the username and password to access the users information

credentials = json.loads(open('credentials.json').read())

# the authentication variable is used to to access the Github API to get necessary data allowing us to make 5000 calls per hour
authentication = HTTPBasicAuth(
    credentials['username'], credentials['password'])

userData = requests.get('https://api.github.com/users/' +
                        credentials['username'], auth=authentication)


userData = userData.json()


print("Information about user {}:\n".format(credentials['username']))
print("Name: {}".format(userData['name']))
print("Email: {}".format(userData['email']))
print("Location: {}".format(userData['location']))
print("Public repos: {}".format(userData['public_repos']))
print("Public gists: {}".format(userData['public_gists']))
print("About: {}".format(userData['bio']))
print("Following: {}".format(userData['following']))
print("Followers: {}".format(userData['followers']))


url = userData['repos_url']
page_no = 1
repos_data = []
while (True):
    response = requests.get(url, auth=authentication)
    response = response.json()
    repos_data = repos_data + response
    repos_fetched = len(response)
    print("Total repositories fetched: {}".format(repos_fetched))
    if (repos_fetched == 30):
        page_no = page_no + 1
        url = userData['repos_url'].encode("UTF-8") + '?page=' + str(page_no)
    else:
        break


repos_information = []
for i, repo in enumerate(repos_data):
    data = []
    data.append(repo['id'])
    data.append(repo['name'])
    data.append(repo['description'])
    data.append(repo['created_at'])
    data.append(repo['updated_at'])
    data.append(repo['owner']['login'])
    data.append(repo['license']['name'] if repo['license'] != None else None)
    data.append(repo['has_wiki'])
    data.append(repo['forks_count'])
    data.append(repo['open_issues_count'])
    data.append(repo['stargazers_count'])
    data.append(repo['watchers_count'])
    data.append(repo['url'])
    data.append(repo['commits_url'].split("{")[0])
    data.append(repo['url'] + '/languages')
    repos_information.append(data)


repos_df = p.DataFrame(repos_information, columns=['Id', 'Name', 'Description', 'Created on', 'Updated on',
                                                   'Owner', 'License', 'Includes wiki', 'Forks count',
                                                   'Issues count', 'Stars count', 'Watchers count',
                                                   'Repo URL', 'Commits URL', 'Languages URL'])

print("Collecting language data")
for i in range(repos_df.shape[0]):
    response = requests.get(
        repos_df.loc[i, 'Languages URL'], auth=authentication)
    response = response.json()
    if response != {}:
        languages = []
        for key, value in response.items():
            languages.append(key)
        languages = ', '.join(languages)
        repos_df.loc[i, 'Languages'] = languages
    else:
        repos_df.loc[i, 'Languages'] = ""
print("Language data collection complete")
